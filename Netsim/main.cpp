

#include "FactoryPackage.hpp"
#include "ReadStructure.hpp"
#include "ReportGeneration.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>
#include <vector>

using namespace std;

std::vector<std::string> parse_line_1(std::string line,char delimiter) {
    std::vector<std::string> tokens;
    std::string token;

    std::istringstream token_stream(line);

    while (std::getline(token_stream, token, delimiter)) {
        if(token.empty())
            continue;
        tokens.push_back(token);
    }

    return tokens;
}


int main () {
    //string line ("x");
    ifstream myfile ("C:/Users/Kamil/Desktop/agh_air_inf2_2018_netsim__2a_2/Netsim/struct-input.txt");
    Factory factory (load_factory_structure(myfile));
    //vector<string> tokens = parse_line_1(line,'=');
    //for (auto x : tokens)
       // cout<<x<<'\n';
    //cout<<tokens.size();
    std::ofstream file("C:/Users/Kamil/Desktop/test.txt");
    generate_structure_report(factory,std::cout);

    return 0;
}


