// 2a_2: Juszczak (297400), Klimek (297408)
//
// Created by Kamil on 28.12.2018.
//
#ifndef NETSIM_REPORTGENERATION_HPP
#define NETSIM_REPORTGENERATION_HPP

#include "FactoryPackage.hpp"

#include <list>
#include <ostream>
#include <functional>
#include <map>

// Kamil Juszczak, 297400

class IReportNotifier{
public:
    virtual bool should_generate_report(Time time) const = 0;
    virtual ~IReportNotifier() = default;
};

class IntervalReportNotifier : public IReportNotifier{
private:
    TimeOffSet _interval;

public:
    IntervalReportNotifier(TimeOffSet interval) : _interval(interval) {}
    bool should_generate_report(Time time) const override;
};

class SpecificTurnsReportNotifier : public IReportNotifier{
private:
    std::list<Time> _specific_turns;

public:
    SpecificTurnsReportNotifier(const std::list<Time>& specific_turns) : _specific_turns(specific_turns) {}
    SpecificTurnsReportNotifier(std::list<Time>&& specific_turns) : _specific_turns(std::move(specific_turns)) {}

    bool should_generate_report(Time time) const override;
};

void generate_structure_report(const Factory& factory,std::ostream& type_of_output);
void generate_simulation_turn(const Factory& factory, std::ostream& type_of_output);
// Kamil Juszczak, 297400




#endif //NETSIM_REPORTGENERATION_HPP
//2a_2: Juszczak (297400), Klimek (297408)