// 2a_2: Juszczak (297400), Klimek (297408)
//
// Created by Kamil on 05.01.2019.
//
#ifndef NETSIM_READSTRUCTURE_HPP
#define NETSIM_READSTRUCTURE_HPP

#include "FactoryPackage.hpp"

#include <map>

// Kamil Juszczak, 297400

//
enum class Tag{
    LOADING_RAMP, WORKER, STOREHOUSE, LINK
};
using FactoryStructure = std::map<Tag,std::vector<std::map<std::string,std::string>>>;

const std::map<std::string,Tag> tags_as_map {
        {std::string("LOADING_RAMP"),Tag::LOADING_RAMP},
        {std::string("WORKER"),Tag ::WORKER},
        {std::string("STOREHOUSE"),Tag ::STOREHOUSE},
        {std::string("LINK"),Tag ::LINK}
};
const std::map<Tag,std::vector<std::string>> node_tags{
    {Tag::LOADING_RAMP, {"id", "delivery-interval"}},
    {Tag::WORKER, {"id", "processing-time", "queue-type"}},
    {Tag::STOREHOUSE, {"id"}},
    {Tag::LINK, {"src","dest"}}
};

FactoryStructure create_factory_structure(std::ifstream& file);

Factory& load_factory_structure(std::ifstream& file);

// Kamil Juszczak, 297400

#endif //NETSIM_READSTRUCTURE_H
// 2a_2: Juszczak (297400), Klimek (297408)