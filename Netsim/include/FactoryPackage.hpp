// Kamil Juszczak, 297400
//
// Created by Kamil on 28.12.2018.
//
#ifndef NETSIM_FACTORYPACKAGE_HPP
#define NETSIM_FACTORYPACKAGE_HPP


#include <list>
#include <utility>
#include <algorithm>
#include <iostream>

#include "NodesPackage.hpp"

using ElementID = int;

template <class Node>
class NodeCollection{

    using iterator = typename  std::list<Node*>::iterator;
    using const_iterator = typename std::list<Node*>::const_iterator;

private:
    std::list<Node*> _node_list;

public:
    NodeCollection() = default;
    NodeCollection(std::list<Node*>& node_list) {
        for (auto& node : node_list){
            _node_list.push_back(std::move(node));
        }
    }
    NodeCollection(NodeCollection& other){
        for (auto node : other._node_list){
            _node_list.push_back(std::move(node));
        }
    };
    NodeCollection(NodeCollection&&) = default;

    const_iterator cbegin() const {return _node_list.cbegin();}
    const_iterator cend() const {return _node_list.cend();}
    iterator begin() { return _node_list.begin();}
    const_iterator begin() const { return _node_list.cbegin();}
    iterator end() { return _node_list.end();}
    const_iterator end() const { return _node_list.cend();}

    void add(Node* node){
        _node_list.push_back(std::move(node));
    }
    const_iterator find_by_id(ElementID id) const{
        return std::find_if(begin(),end(),[&](const Node* node) { return node->getID() == id;});
    }
    iterator find_by_id(ElementID id){
        return std::find_if(begin(),end(),[&](const Node* node) { return node->getID() == id;});
    }
    void remove_by_id(ElementID id){
        auto result = find_by_id(id);
        if (result != end())
            _node_list.erase(result);
    }
};

using Ramps = NodeCollection<Ramp>;
using Workers = NodeCollection<Worker>;
using Storehouses = NodeCollection<Storehouse>;


class Factory{
private:
    Workers _workers;
    Ramps _ramps;
    Storehouses _storehouses;

    void remove_connections(PackageSender* package_sender, IPackageReceiver* receiver);
    bool check_worker_connections(IPackageReceiver* iPackageReceiver) const;

public:
    Factory(Workers& workers, Ramps& ramps, Storehouses& storehouses) : _workers(workers), _ramps(ramps),
                                                                        _storehouses(storehouses) {}
    Factory(Workers&& workers, Ramps&& ramps, Storehouses&& storehouses) : _workers(std::move(workers)),
                                                                           _ramps(std::move(ramps)), _storehouses(std::move(storehouses)) {}

    std::list<Worker*>::const_iterator get_worker_by_id(ElementID id) const { return _workers.find_by_id(id); };
    std::list<Ramp*>::const_iterator get_ramp_by_id(ElementID id) const { return _ramps.find_by_id(id); };
    std::list<Storehouse*>::const_iterator get_storehouse_by_id(ElementID id) const { return _storehouses.find_by_id(id); } ;

    void remove_worker_by_id(ElementID id);
    void remove_ramp_by_id(ElementID id)  {_ramps.remove_by_id(id);};
    void remove_storehouse_by_id(ElementID id);

    void add_worker(Worker* worker) { _workers.add(worker); };
    void add_ramp(Ramp* ramp) { _ramps.add(ramp); };
    void add_storehouse(Storehouse* storehouse) { _storehouses.add(storehouse); };

    std::list<Worker*>::const_iterator wor_cbegin() const {return _workers.cbegin();};
    std::list<Worker*>::const_iterator wor_cend() const {return _workers.cend();};

    std::list<Ramp*>::const_iterator ramp_cbegin() const { return _ramps.cbegin();};
    std::list<Ramp*>::const_iterator ramp_cend() const { return _ramps.cend();};

    std::list<Storehouse*>::const_iterator store_cbegin() const { return _storehouses.cbegin();};
    std::list<Storehouse*>::const_iterator store_cend() const { return _storehouses.cend();};
    bool check_sim() const;
};


#endif //NETSIM_FACTORYPACKAGE_H
// Kamil Juszczak, 297400