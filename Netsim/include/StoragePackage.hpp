//
// Created by pawel on 03.01.2019.
//

//2a_2 Nowak, <297434>


#ifndef NETSIM_STORAGEPACKAGE_HPP
#define NETSIM_STORAGEPACKAGE_HPP

#include <map>
#include <deque>
using ElementID = int;

class Package{
private:
    ElementID _id;
public:
    static ElementID current_id;
    Package() {
        _id = current_id;
        current_id++;
    };
    ElementID get_id() const {return _id;};
};
//---------------------------------------------------------------------
enum class QueueType {
    LIFO, FIFO
};
const std::map<std::string,QueueType > queue_type_as_string {
    {std::string("FIFO"),QueueType::FIFO},
    {std::string("LIFO"),QueueType::LIFO},
    };

//---------------------------------------------------------------------
class IPackageStockpile {
    using const_iterator = typename std::deque<Package>::const_iterator;

public:
    virtual const_iterator cbegin()const=0;
    virtual const_iterator cend()const=0;
    virtual const_iterator begin()=0;
    virtual const_iterator end()=0;
    virtual void add_package(Package package)=0;
    virtual ~IPackageStockpile()=default;
};
//---------------------------------------------------------------------
class IPackageQueue : public IPackageStockpile {
public:
    //virtual Package choose_package()=0;
    virtual QueueType get_queue_type()=0;
    virtual ~IPackageQueue()=default;
};
//---------------------------------------------------------------------
class PackageQueue : public  IPackageQueue {
    using const_iterator = typename std::deque<Package>::const_iterator;

private:
    std::deque<Package> _package_queue;
    QueueType _type_of_queue;
public:
    PackageQueue(std::deque<Package> package_queue, QueueType type_of_queue): _package_queue(package_queue), _type_of_queue(type_of_queue){};
    PackageQueue(QueueType type_of_queue) : _type_of_queue(type_of_queue) {}
    PackageQueue(PackageQueue&& other) : _package_queue(other._package_queue),_type_of_queue(other._type_of_queue) {}
    PackageQueue(PackageQueue& other) : _package_queue(other._package_queue), _type_of_queue(other._type_of_queue) {}
    //Package choose_package() override {};
    QueueType get_queue_type() override {return _type_of_queue;};
    void add_package(Package package) override {_package_queue.push_back(package);};

    const_iterator cbegin() const override {return _package_queue.cbegin();};
    const_iterator cend() const override{return _package_queue.cend();};
    const_iterator begin() override {return _package_queue.begin();};
    const_iterator end() override {return _package_queue.end();};
    };
//2a_2 Nowak, <297434>
#endif //NETSIM_STORAGEPACKAGE_HPP
