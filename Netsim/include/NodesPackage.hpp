//2a_2: Nowak, 297434, Klimek, 297408
//
// Created by Kamil on 06.01.2019.
//

#ifndef NETSIM_NODESPACKAGE_HPP
#define NETSIM_NODESPACKAGE_HPP


#include "StoragePackage.hpp"

#include <memory>
#include <optional>
#include <map>
#include <iostream>
#include <functional>
#include <list>




//2a_2: Klimek, 297408
enum class ReceiverType{
    Worker, Storehouse
};


class IPackageReceiver {
    using const_iterator = typename std::deque<Package>::const_iterator;
public:
    virtual void addPackage(Package package)=0;
    virtual ReceiverType getReceiverType() const =0;
    virtual ElementID getID() const = 0;
    virtual const_iterator cbegin() const =0;
    virtual const_iterator cend() const =0;
    virtual const_iterator begin()=0;
    virtual const_iterator end()=0;

    virtual ~IPackageReceiver() = default;
};
using ProbabilityValue = double;
using ProbabilityGenerator = std::function<ProbabilityValue(std::list<IPackageReceiver*>)>;


class ReceiverPreferences{
    using const_iterator = typename std::map<IPackageReceiver*, ProbabilityValue>::const_iterator;
private:
    std::map<IPackageReceiver*, ProbabilityValue> _preferences;
    void scaleProbability();
public:
    ReceiverPreferences(std::list<IPackageReceiver*> receiversList, ProbabilityGenerator foo){
        for (auto receiver : receiversList){
            _preferences[receiver] = foo(receiversList);
        }
    };
    ReceiverPreferences() = default;
    void show_receivers(){
        for (auto rec : _preferences){
            if(rec.first->getReceiverType() == ReceiverType::Worker)
                std::cout<<"WORKER "<<rec.first->getID()<<'\n';
            else
                std::cout<<"STOREHOUSE "<<rec.first->getID()<<'\n';
        }
    }
    void addReceiver(IPackageReceiver* receiver);
    void deleteReceiver(IPackageReceiver* receiver);
    IPackageReceiver* chooseReceiver();

    const_iterator cbegin() const {return _preferences.cbegin();};
    const_iterator cend() const {return _preferences.cend();};
    const_iterator begin() const {return _preferences.begin();};
    const_iterator end() const { return _preferences.end();};
};

class PackageSender{
protected:
    std::optional<Package> _package_bufor;
public:
    ReceiverPreferences _receiverPreferences;
    PackageSender(ReceiverPreferences&& receiverPreferences) : _receiverPreferences(receiverPreferences) {}
    PackageSender(ReceiverPreferences& receiverPreferences) : _receiverPreferences(receiverPreferences) {}
    PackageSender() : _receiverPreferences() {}
    void put_package_to_bufor(Package package){
        _package_bufor.emplace(package);
    };
    void sendPackage();
    std::optional<Package> getBufor(){
        return _package_bufor;
    }
    void addReceiverPreferences(ReceiverPreferences& receiverPreferences) {_receiverPreferences = receiverPreferences;}
};
//2a_2: Klimek, 297408

//2a_2: Nowak, 297434,
using TimeOffSet = int;
using Time = int;

class Ramp :public PackageSender {
private:
    ElementID _id;
    TimeOffSet _delivery_interval;
public:
    Ramp(ElementID id, TimeOffSet delivery_interval, ReceiverPreferences&& receiver_preferences): PackageSender(receiver_preferences), _id(id), _delivery_interval(delivery_interval) {}
    Ramp(ElementID id, TimeOffSet delivery_interval) : PackageSender(), _id(id), _delivery_interval(delivery_interval) {}
    void deliver_package();
    TimeOffSet get_delivery_interval() const {return _delivery_interval;};
    ElementID getID() const {return _id;};
};

class Storehouse :public IPackageReceiver {
    using const_iterator = typename std::deque<Package>::const_iterator;
private:
    ElementID _id;
    std::unique_ptr<IPackageStockpile> _stockpile_ptr;
public:
    Storehouse(ElementID id, std::unique_ptr<IPackageStockpile> stockpile_ptr): _id(id), _stockpile_ptr(std::move(stockpile_ptr)) {}
    Storehouse(Storehouse& other): _id(other._id),  _stockpile_ptr(std::move(other._stockpile_ptr))  {}
    Storehouse(Storehouse && other): _id(other._id), _stockpile_ptr(std::move(other._stockpile_ptr)) {}
    Storehouse(ElementID id) : _id(id) {} // do kasacji

    void addPackage(Package package) override {_stockpile_ptr->add_package(package);};
    ReceiverType getReceiverType() const override {return ReceiverType::Storehouse;};
    ElementID getID() const override {return _id;};
    const_iterator cbegin() const override {return _stockpile_ptr->cbegin();};
    const_iterator cend() const override {return _stockpile_ptr->cend();};
    const_iterator begin() override {return _stockpile_ptr->begin();};
    const_iterator end() override {return _stockpile_ptr->end();};
};


class Worker :public IPackageReceiver, public PackageSender {
    using const_iterator = typename std::deque<Package>::const_iterator;
private:
    ElementID _id;
    Time _time;
    Time _processing_time;
    std::unique_ptr<IPackageQueue> _queue_ptr;
    std::optional<Package> _bufor;
public:
    Worker(ElementID id, Time time, std::unique_ptr<IPackageQueue> queue_ptr, ReceiverPreferences receiver_preferences):  PackageSender(receiver_preferences), _id(id), _time(time),_processing_time(1), _queue_ptr(std::move(queue_ptr)) {}
    Worker(Worker& other):  PackageSender(other._receiverPreferences), _id(other._id), _time(other._time), _processing_time(other._processing_time), _queue_ptr(std::move(other._queue_ptr)) {}
    //kasacja to na dole
    Worker(Worker && other):  PackageSender(other._receiverPreferences), _id(other._id), _time(other._time),_processing_time(other._processing_time), _queue_ptr(std::move(other._queue_ptr)), _bufor(other._bufor) {}
    //kasacja to u gory
    Worker(ElementID id, Time time,std::unique_ptr<IPackageQueue> queue_ptr,Package&& package) : PackageSender(), _id(id), _time(time),_processing_time(1), _queue_ptr(std::move(queue_ptr)), _bufor(package) {}
    Worker(ElementID id, Time time) : PackageSender(), _id(id), _time(time) {}
    Worker(ElementID id, Time time, ReceiverPreferences&& receiverPreferences) : PackageSender(receiverPreferences), _id(id), _time(time),_processing_time(1) {}

    ElementID getCurrentPackageID() {return _bufor->get_id();};
    void addPackage(Package package) override {_queue_ptr->add_package(package);};
    ReceiverType getReceiverType() const override {return ReceiverType::Worker;};
    ElementID getID() const override { return _id;};
    Time get_time() const {return _time;};
    Time get_processing_time() const {return _processing_time;};
    QueueType get_queue_type() const {return _queue_ptr->get_queue_type();};
    void working_process();
    const_iterator cbegin() const override { return _queue_ptr->cbegin();};
    const_iterator cend() const override { return _queue_ptr->cend();};
    const_iterator begin() override { return _queue_ptr->begin();};
    const_iterator end() override { return _queue_ptr->end();};
};


//2a_2: Nowak, 297434,
//2a_2: Nowak, 297434, Klimek, 297408
#endif //NETSIM_NODESPACKAGE_HPP
