// Kamil Juszczak, 297400
//
// Created by Kamil on 12.01.2019.
//

#include "FactoryPackage.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <memory>



using ::testing::Return;


TEST(NodeColleciton, addNode_1){
    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);
    Storehouse storehouse_3 = Storehouse(3);
    Storehouse storehouse_4 = Storehouse(4);

    std::list<Storehouse*> storehouses_list {&storehouse_1,&storehouse_2,&storehouse_3,&storehouse_4};

    NodeCollection<Storehouse> storehouses (storehouses_list);

    EXPECT_EQ((*storehouses.find_by_id(2)),&storehouse_2);

}

ProbabilityValue probability_generator_test(std::list<IPackageReceiver*> list_of_receiver){
    if (list_of_receiver.size() != 0)
        return 1/list_of_receiver.size();
    else return 0;
}

TEST(NodeCollection, addNode_2){
    ProbabilityGenerator prob_generator = probability_generator_test;

    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);

    std::list<IPackageReceiver*> rec_pointers_list_w1 = {};
    Worker worker_1 = Worker(1,2,ReceiverPreferences(rec_pointers_list_w1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w2 = {&storehouse_1,&worker_1};
    Worker worker_2 = Worker(2,3,ReceiverPreferences(rec_pointers_list_w2,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w3 = {&worker_2,&storehouse_2};
    Worker worker_3 = Worker(3,4,ReceiverPreferences(rec_pointers_list_w3,prob_generator));

    std::list<Worker*> worker_list = {&worker_1,&worker_2,&worker_3};

    NodeCollection<Worker> workers (worker_list);

    EXPECT_EQ((*workers.find_by_id(2)),&worker_2);

}

TEST(NodeCollection,RemoveNode_1){
    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);
    Storehouse storehouse_3 = Storehouse(3);
    Storehouse storehouse_4 = Storehouse(4);

    std::list<Storehouse*> storehouses_list {&storehouse_1,&storehouse_2,&storehouse_3,&storehouse_4};

    NodeCollection<Storehouse> storehouses (storehouses_list);

    storehouses.remove_by_id(1);

    EXPECT_EQ(((*storehouses.find_by_id(1))==&storehouse_1),false);

}

TEST(NodeCollection, removeNode_2){
    ProbabilityGenerator prob_generator = probability_generator_test;

    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);

    std::list<IPackageReceiver*> rec_pointers_list_w1 = {};
    Worker worker_1 = Worker(1,2,ReceiverPreferences(rec_pointers_list_w1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w2 = {&storehouse_1,&worker_1};
    Worker worker_2 = Worker(2,3,ReceiverPreferences(rec_pointers_list_w2,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w3 = {&worker_2,&storehouse_2};
    Worker worker_3 = Worker(3,4,ReceiverPreferences(rec_pointers_list_w3,prob_generator));

    std::list<Worker*> worker_list = {&worker_1,&worker_2,&worker_3};

    NodeCollection<Worker> workers (worker_list);

    workers.remove_by_id(3);

    EXPECT_EQ(((*workers.find_by_id(3))==&worker_3),false);

}

TEST(FactoryTest, checksim) {
    ProbabilityGenerator prob_generator = probability_generator_test;

    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);

    std::list<IPackageReceiver*> rec_pointers_list_w1 = {};
    Worker worker_1 = Worker(1,2,ReceiverPreferences(rec_pointers_list_w1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w2 = {&storehouse_1,&worker_1};
    Worker worker_2 = Worker(2,3,ReceiverPreferences(rec_pointers_list_w2,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w3 = {&worker_2,&storehouse_2};
    Worker worker_3 = Worker(3,4,ReceiverPreferences(rec_pointers_list_w3,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r1 = {&worker_2};
    Ramp ramp_1 = Ramp(1,2,ReceiverPreferences(rec_pointers_list_r1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r2 = {&worker_2,&worker_3};
    Ramp ramp_2 = Ramp(2,1,ReceiverPreferences(rec_pointers_list_r2,prob_generator));


    std::list<Worker*> worker_list = {&worker_1,&worker_2,&worker_3};
    std::list<Storehouse*> storehouse_list {&storehouse_1,&storehouse_2};
    std::list<Ramp*> ramp_list = {&ramp_1,&ramp_2};

    Factory factory = Factory(Workers(worker_list),Ramps(ramp_list),Storehouses(storehouse_list));



    EXPECT_FALSE(factory.check_sim());

}

TEST(FactoryTest,remove_worker){
    ProbabilityGenerator prob_generator = probability_generator_test;

    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);

    std::list<IPackageReceiver*> rec_pointers_list_w1 = {&storehouse_1};
    Worker worker_1 = Worker(1,2,ReceiverPreferences(rec_pointers_list_w1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w2 = {&storehouse_1,&worker_1};
    Worker worker_2 = Worker(2,3,ReceiverPreferences(rec_pointers_list_w2,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w3 = {&worker_2,&storehouse_2};
    Worker worker_3 = Worker(3,4,ReceiverPreferences(rec_pointers_list_w3,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r1 = {&worker_2};
    Ramp ramp_1 = Ramp(1,2,ReceiverPreferences(rec_pointers_list_r1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r2 = {&worker_2,&worker_3};
    Ramp ramp_2 = Ramp(2,1,ReceiverPreferences(rec_pointers_list_r2,prob_generator));


    std::list<Worker*> worker_list = {&worker_1,&worker_2,&worker_3};
    std::list<Storehouse*> storehouse_list {&storehouse_1,&storehouse_2};
    std::list<Ramp*> ramp_list = {&ramp_1,&ramp_2};

    Factory factory = Factory(Workers(worker_list),Ramps(ramp_list),Storehouses(storehouse_list));

    factory.remove_worker_by_id(1);
    EXPECT_EQ(factory.check_sim(),true);
}

TEST(FactoryTest,remove_storehouse){
    ProbabilityGenerator prob_generator = probability_generator_test;

    Storehouse storehouse_1 = Storehouse(1);
    Storehouse storehouse_2 = Storehouse(2);

    std::list<IPackageReceiver*> rec_pointers_list_w1 = {&storehouse_1};
    Worker worker_1 = Worker(1,2,ReceiverPreferences(rec_pointers_list_w1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w2 = {&storehouse_1,&worker_1};
    Worker worker_2 = Worker(2,3,ReceiverPreferences(rec_pointers_list_w2,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_w3 = {&worker_2,&storehouse_2};
    Worker worker_3 = Worker(3,4,ReceiverPreferences(rec_pointers_list_w3,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r1 = {&worker_2};
    Ramp ramp_1 = Ramp(1,2,ReceiverPreferences(rec_pointers_list_r1,prob_generator));

    std::list<IPackageReceiver*> rec_pointers_list_r2 = {&worker_2,&worker_3};
    Ramp ramp_2 = Ramp(2,1,ReceiverPreferences(rec_pointers_list_r2,prob_generator));


    std::list<Worker*> worker_list = {&worker_1,&worker_2,&worker_3};
    std::list<Storehouse*> storehouse_list {&storehouse_1,&storehouse_2};
    std::list<Ramp*> ramp_list = {&ramp_1,&ramp_2};

    Factory factory = Factory(Workers(worker_list),Ramps(ramp_list),Storehouses(storehouse_list));

    factory.remove_storehouse_by_id(1);

    EXPECT_EQ(factory.check_sim(),false);
}

// Kamil Juszczak, 297400