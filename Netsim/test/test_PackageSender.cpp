//2a_2: Klimek, 297408
//
// Created by Dominik Klimek on 13.01.2019.
//
/*
#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include "NodesPackage.hpp"
#include <list>

class MockPackageReceiver: public IPackageReceiver{
    using const_iterator = typename std::deque<Package>::const_iterator;
public:
    MOCK_METHOD0(getReceiverType, std::pair<ReceiverType, ElementID>());
    MOCK_METHOD1(addPackage, void(Package package));
    MOCK_METHOD0(cbegin, const_iterator());
    MOCK_METHOD0(cend, const_iterator());
    MOCK_METHOD0(begin, const_iterator());
    MOCK_METHOD0(end, const_iterator());
};

TEST(PackageSender, PackageSend){
    MockPackageReceiver mock_PackageReceiver;
    ProbabilityGenerator testFun = []() { return 1/2;};
    Package package;
    MockPackageReceiver* receiver1;
    MockPackageReceiver* receiver2;
    std::list<IPackageReceiver*> receivers {receiver1,receiver2};
    ReceiverPreferences recPref = ReceiverPreferences(receivers, testFun);

    PackageSender packSend = PackageSender(recPref, package);
    packSend.sendPackage();

    EXPECT_FALSE(packSend.getBufor());
}
 */
//2a_2: Klimek, 297408