// 2a_2: Juszczak (297400), Klimek (297408)
//
// Created by Kamil on 17.01.2019.
//

#include "ReadStructure.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <iostream>
#include <fstream>

// Kamil Juszczak, 297400
std::ifstream myfile ("C:/Users/Kamil/Desktop/agh_air_inf2_2018_netsim__2a_2/Netsim/struct-input.txt");

using ::testing::Return;

TEST(LoadStructure, create_structure){
    Factory factory (load_factory_structure(myfile));

    EXPECT_TRUE(factory.check_sim());
}
// Kamil Juszczak, 297400


// 2a_2: Juszczak (297400), Klimek (297408)