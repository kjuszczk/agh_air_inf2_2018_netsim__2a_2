// 2a_2: Juszczak (297400), Klimek (297408)

//
// Created by Kamil on 19.01.2019.
//
#include "ReportGeneration.hpp"
#include "ReadStructure.hpp"

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <iostream>
#include <fstream>


// Kamil Juszczak, 297400
std::ifstream my_file ("C:/Users/Kamil/Desktop/agh_air_inf2_2018_netsim__2a_2/Netsim/struct-input.txt");

using ::testing::Return;

TEST(GenerateReport,generate_report){
    Factory factory (load_factory_structure(my_file));

    std::cout<<"Generate structure report \n";
    generate_structure_report(factory,std::cout);
    std::cout<<"\nGenerate simulation turn \n";
    generate_simulation_turn(factory,std::cout);


}
// Kamil Juszczak, 297400

// 2a_2: Juszczak (297400), Klimek (297408)