//
// Created by Dominik Klimek on 13.01.2019.
//

#include "NodesPackage.hpp"
#include <cstdlib>
#include <ctime>
#include <functional>

void ReceiverPreferences::addReceiver(IPackageReceiver *receiver) {
    _preferences.insert(std::pair<IPackageReceiver*, ProbabilityValue>(receiver, 0));
    scaleProbability();
}

void ReceiverPreferences::deleteReceiver(IPackageReceiver *receiver) {
    _preferences.erase(receiver);
    scaleProbability();
}

IPackageReceiver* ReceiverPreferences::chooseReceiver() {
    IPackageReceiver* receiver_ptr;
    srand((int)time(0));
    //std::function<double()> randomValue = []() {return (rand() % 100)/100;}; - poprawna implementacja
    std::function<double()> randomValue = []() { return 1/3;}; // implementacja do testów z mockowaniem
    double randomVal = randomValue();
    double sum = 0;
    for (auto i = cbegin(); i != cend(); i++){
        sum += (*i).second;
        if (sum >= randomVal) {
            receiver_ptr = (*i).first;
            break;
        }
    }
    return receiver_ptr;
}

void ReceiverPreferences::scaleProbability() {
    for (auto& elem : _preferences){
        elem.second = 1/_preferences.size();
    }
}

void PackageSender::sendPackage() {
    _receiverPreferences.chooseReceiver() -> addPackage(_package_bufor.value());
    _package_bufor.reset();
}