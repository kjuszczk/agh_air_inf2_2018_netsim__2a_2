// Kamil Juszczak, 297400
//
// Created by Kamil on 06.01.2019.
//
#include "FactoryPackage.hpp"
#include "iostream"

void Factory::remove_connections(PackageSender* package_sender, IPackageReceiver* receiver) {
    package_sender->_receiverPreferences.deleteReceiver(receiver);
}

void Factory::remove_worker_by_id(ElementID id) {
    for(auto it = _ramps.begin(); it != _ramps.end(); it++){
        remove_connections(*it,(*_workers.find_by_id(id)));
    }
    for(auto it = _workers.begin(); it != _workers.end(); it++){
        remove_connections(*it,(*_workers.find_by_id(id)));
    }
    _workers.remove_by_id(id);
}

void Factory::remove_storehouse_by_id(ElementID id) {
    for(auto it = _workers.begin(); it != _workers.end(); it++){
        remove_connections(*it,*_storehouses.find_by_id(id));
    }
    _storehouses.remove_by_id(id);
}


bool Factory::check_worker_connections(IPackageReceiver* iPackageReceiver) const {

    bool flag = false;

    if(iPackageReceiver->getReceiverType()==ReceiverType::Worker) {
        auto it_worker = get_worker_by_id(iPackageReceiver->getID());
        for (auto it = (*it_worker)->_receiverPreferences.cbegin();
             it != (*it_worker)->_receiverPreferences.cend(); it++) {
            if (it->first->getReceiverType() == ReceiverType::Worker && iPackageReceiver->getID() != it->first->getID()) {
                flag = check_worker_connections(it->first);
                break;
            }
            if (it->first->getReceiverType() == ReceiverType::Storehouse) {
                flag = true;
            }
        }
    }
    else {
        flag = true;
    }
    return flag;
}

bool Factory::check_sim() const {
    for (auto it_r = _ramps.cbegin() ; it_r != _ramps.cend() ; it_r++){
        for (auto it_w = (*it_r)->_receiverPreferences.cbegin() ; it_w != (*it_r)->_receiverPreferences.cend() ; it_w ++){
            if(check_worker_connections(it_w->first)==false) {
                return false;
            }
        }
    }
    return true;
}
// Kamil Juszczak, 297400