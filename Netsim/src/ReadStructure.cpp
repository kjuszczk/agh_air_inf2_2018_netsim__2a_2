// 2a_2: Juszczak (297400), Klimek (297408)
//
// Created by Kamil on 15.01.2019.
//

#include "ReadStructure.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <sstream>




#include <sstream>
#include <iostream>
#include <vector>

// Kamil Juszczak, 297400
std::vector<std::string> parse_line(std::string line,char delimiter) {
    std::vector<std::string> tokens;
    std::string token;

    std::istringstream token_stream(line);

    while (std::getline(token_stream, token, delimiter)) {
        if(token.empty())
            continue;
        tokens.push_back(token);
    }

    return tokens;
}

FactoryStructure create_factory_structure(std::ifstream& file) {
    std::string line;

    FactoryStructure factory_struct;
    if (file.is_open()) {
        while (std::getline(file, line)) {
            if (line[0] == ';' || line.empty() || line[0] == ' ')
                continue;

            std::vector<std::string> tokens = parse_line(line,' ');
            std::string tag = tokens[0];
            tokens.erase(tokens.begin());
            auto it_tags = tags_as_map.find(tag);
            if (it_tags == tags_as_map.cend()){
                throw std::runtime_error("Some tag does not exist");
            }

            if (it_tags != tags_as_map.cend()) {
                std::vector<std::map<std::string,std::string>> vector_of_tokens;
                if (factory_struct.find(it_tags->second) == factory_struct.end()) {
                    std::map<std::string,std::string> node_struct;
                    for (auto x : tokens) {
                        std::vector<std::string> node_tokens = parse_line(x, '=');
                        node_struct[node_tokens[0]] = node_tokens[1];
                    }
                    vector_of_tokens.push_back(node_struct);
                    factory_struct[it_tags->second] = vector_of_tokens;
                }
                else{
                    std::map<std::string,std::string> node_struct;
                    for (auto x : tokens) {
                        std::vector<std::string> node_tokens = parse_line(x, '=');
                        node_struct[node_tokens[0]] = node_tokens[1];
                    }
                    factory_struct[it_tags->second].push_back(node_struct);
                }

            }

        }
        file.close();
    }
    return factory_struct;
}

bool check_node_structure(FactoryStructure& factory_structure,Tag tag ,const std::vector<std::string> string_list){

    auto it_tag = factory_structure.find(tag);

    for(auto node : it_tag->second){
        for(auto string : string_list){
            if (node.find(string) == node.end())
                return false;
        }
    }
    return true;
}

void check_factory_structure(FactoryStructure& factory_structure){
    for (auto& tag : factory_structure){
        if(!check_node_structure(factory_structure,tag.first,node_tags.find(tag.first)->second))
            throw std::runtime_error("Cannot create Factory");
    }
}

Factory& load_factory_structure(std::ifstream& file){

    FactoryStructure factory_structure = create_factory_structure(file);
    check_factory_structure(factory_structure);

    auto links_from_file = factory_structure.find(Tag::LINK);
    std::vector<std::pair<ElementID,ElementID>> workers_link_with_workers;
    std::vector<std::pair<ElementID,ElementID>> workers_link_with_store;
    std::vector<std::pair<ElementID,ElementID>> ramps_link_with_workers;

    for (auto link : links_from_file->second){
        std::vector<std::string> tokens_src = parse_line(link[std::string("src")],'-');
        std::vector<std::string> tokens_dest = parse_line(link[std::string("dest")],'-');
        if (tokens_src[0] == std::string("ramp")){
            ramps_link_with_workers.push_back(std::make_pair(std::stoi(tokens_src[1]),std::stoi(tokens_dest[1])));
            continue;
        }
        if (tokens_dest[0] == std::string("store")){
            workers_link_with_store.push_back(std::make_pair(std::stoi(tokens_src[1]),std::stoi(tokens_dest[1])));
            continue;
        }
        if(tokens_dest[0]==std::string("worker")&&tokens_src[0]==std::string("worker")) {
            workers_link_with_workers.push_back(std::make_pair(std::stoi(tokens_src[1]), std::stoi(tokens_dest[1])));
            continue;
        }
        throw std::runtime_error("Cannot create connection");
    }

    Storehouses storehouses;
    auto store_from_file = factory_structure.find(Tag::STOREHOUSE);
    for(auto store : store_from_file->second){
        ElementID id = std::stoi(store.find(std::string("id"))->second);
        QueueType queue_type = QueueType::FIFO;
        auto pointer_s = new Storehouse(id,std::make_unique<PackageQueue>(queue_type));
        pointer_s->addPackage(Package());
        pointer_s->addPackage(Package());
        storehouses.add(pointer_s);
    }

    Workers workers;
    auto workers_from_file = factory_structure.find(Tag::WORKER);
    for(auto worker : workers_from_file->second){

        ElementID id = std::stoi(worker.find(std::string("id"))->second);
        Time processing_time = std::stoi(worker.find(std::string("processing-time"))->second);
        QueueType queue_type = queue_type_as_string.find(worker.find(std::string("queue-type"))->second)->second;
        auto pointer_w = new Worker(id,processing_time,std::make_unique<PackageQueue>(queue_type),Package());
        pointer_w->addPackage(Package());
        workers.add(pointer_w);
    }

    Ramps ramps;
    auto ramps_from_file = factory_structure.find(Tag::LOADING_RAMP);
    for(auto ramp : ramps_from_file->second){
        ElementID id = std::stoi(ramp.find(std::string("id"))->second);
        Time processing_time = std::stoi(ramp.find(std::string("delivery-interval"))->second);
        auto pointer_r = new Ramp(id,processing_time);
        ramps.add(pointer_r);
    }

    for (auto src : workers_link_with_store){
        auto it_store = storehouses.find_by_id(src.second);
        auto it_worker = workers.find_by_id(src.first);
        if (it_store != storehouses.end() && it_worker != workers.end())
            (*it_worker)->_receiverPreferences.addReceiver((*it_store));
        else
            throw std::runtime_error("Could not find node by id");
    }
    for (auto src : workers_link_with_workers){
        auto it_worker_dest = workers.find_by_id(src.second);
        auto it_worker_src = workers.find_by_id(src.first);
        if (it_worker_src != workers.end() && it_worker_dest != workers.end())
            (*it_worker_src)->_receiverPreferences.addReceiver((*it_worker_dest));
        else
            throw std::runtime_error("Could not find node by id");
    }
    for (auto src : ramps_link_with_workers){
        auto it_worker = workers.find_by_id(src.second);
        auto it_ramp = ramps.find_by_id(src.first);
        if (it_ramp != ramps.end() && it_worker != workers.end())
            (*it_ramp)->_receiverPreferences.addReceiver((*it_worker));
        else
            throw std::runtime_error("Could not find node by id");
    }

    auto factory_pointer = new Factory(workers,ramps,storehouses);
    return *factory_pointer;
}
// Kamil Juszczak, 297400

// 2a_2: Juszczak (297400), Klimek (297408)