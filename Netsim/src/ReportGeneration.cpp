// 2a_2: Juszczak (297400), Klimek (297408)
//
// Created by Kamil on 16.01.2019.
//
#include "ReportGeneration.hpp"
#include "ReadStructure.hpp"

#include <sstream>

// Kamil Juszczak, 297400
bool IntervalReportNotifier::should_generate_report(Time time) const {
    if (time% _interval == 1)
        return true;
    return false;
}

bool SpecificTurnsReportNotifier::should_generate_report(Time time) const {

    for (auto& specific_turn : _specific_turns){
        if(specific_turn == time)
            return true;
    }
    return false;
}

std::string receiver_type_to_string (ReceiverType type){
    if(type == ReceiverType::Worker)
        return std::string("worker");
    return std::string("storehouse");
}

std::string queue_type_to_string(QueueType type){
    if(type == QueueType::FIFO)
        return std::string("FIFO");
    return std::string("LIFO");
}

void generate_structure_report(const Factory& factory,std::ostream& type_of_output) {

    std::vector<std::string> storehouses_lines;
    std::vector<std::string> workers_lines;
    std::vector<std::string> ramps_lines;

    for (auto it_worker = factory.wor_cbegin(); it_worker != factory.wor_cend(); it_worker++) {
        std::string line;
        std::ostringstream oss;
        oss << "WORKER #" << (*it_worker)->getID() << "\n  Processing time: " << (*it_worker)->get_time()
            << "\n  Queue type: " << queue_type_to_string((*it_worker)->get_queue_type()) << "\n  Receivers:\n";
        for (auto it_rec = (*it_worker)->_receiverPreferences.cbegin();
             it_rec != (*it_worker)->_receiverPreferences.cend(); it_rec++) {
            oss << "    " << receiver_type_to_string(it_rec->first->getReceiverType()) << " #" << it_rec->first->getID()
                << '\n';
        }
        oss<<'\n';
        workers_lines.push_back(oss.str());
    }
    for (auto it_ramp = factory.ramp_cbegin(); it_ramp != factory.ramp_cend(); it_ramp++) {
        std::string line;
        std::ostringstream oss;
        oss << "LOADING_RAMP #" << (*it_ramp)->getID() << "\n  Delivery interval: "
            << (*it_ramp)->get_delivery_interval() << "\n  Receivers:\n";
        for (auto it_rec = (*it_ramp)->_receiverPreferences.cbegin();
             it_rec != (*it_ramp)->_receiverPreferences.cend(); it_rec++) {
            oss << "    " << receiver_type_to_string(it_rec->first->getReceiverType()) << " #" << it_rec->first->getID()
                << '\n';
        }
        oss<<'\n';
        ramps_lines.push_back(oss.str());
    }

    for(auto it_store = factory.store_cbegin(); it_store != factory.store_cend(); it_store++){
        std::string line;
        std::ostringstream oss;
        oss<<"STOREHOUSE #"<<(*it_store)->getID()<<'\n';

        storehouses_lines.push_back(oss.str());
    }

    std::ostringstream oss;
    oss<<"== LOADING_RAMP ==\n\n";
    for (auto line : ramps_lines)
        oss<<line;
    oss<<"\n== WORKERS ==\n\n";
    for (auto line : workers_lines)
        oss<<line;
    oss<<"\n== STOREHOUSES ==\n\n";
    for (auto line : storehouses_lines)
        oss<<line;
    type_of_output<<oss.str();


}


void generate_simulation_turn(const Factory& factory, std::ostream& type_of_output){
    std::vector<std::string> storehouses_lines;
    std::vector<std::string> workers_lines;

    for (auto it_worker = factory.wor_cbegin(); it_worker != factory.wor_cend(); it_worker++) {
        std::string line;
        std::ostringstream oss;
        oss << "WORKER #" << (*it_worker)->getID() << "\n  Queue: #"<<(*it_worker)->getCurrentPackageID()<<" (pt = "<<(*it_worker)->get_processing_time()<<" )";
        for (auto it_pac = (*it_worker)->cbegin(); it_pac != (*it_worker)->cend(); it_pac++){
            oss << ", #" <<it_pac->get_id();
        }
        oss<<'\n';
        workers_lines.push_back(oss.str());
    }

    for(auto it_store = factory.store_cbegin(); it_store != factory.store_cend(); it_store++){
        std::string line;
        std::ostringstream oss;
        oss<<"STOREHOUSE #"<<(*it_store)->getID()<<"\n  Queue:";
        for (auto it_pac = (*it_store)->cbegin(); it_pac != (*it_store)->cend(); it_pac++){
            oss << ", #" <<it_pac->get_id();
        }
        storehouses_lines.push_back(oss.str());
    }
    std::ostringstream oss;
    oss<<"== WORKERS ==\n\n";
    for (auto line : workers_lines)
        oss<<line;
    oss<<"\n\n== STOREHOUSES ==\n\n";
    for (auto line : storehouses_lines)
        oss<<line;

    type_of_output<<oss.str();

}
// Kamil Juszczak, 297400

// 2a_2: Juszczak (297400), Klimek (297408)